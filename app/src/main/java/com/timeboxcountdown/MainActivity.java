package com.timeboxcountdown;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private GestureDetectorCompat _gestureDetectorCompat = null;
    private FragmentManager fm = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SlideListener slideListener = new SlideListener(this);
        _gestureDetectorCompat = new GestureDetectorCompat(this, slideListener);

        SettingsFragment _settingsFragment = SettingsFragment.newInstance("mySettingsFragment");
        final CountdownFragment _countdownFragment = CountdownFragment.newInstance("myCountdownFragment");

        fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.flFragment, _settingsFragment)
                .commit();


        Button startButton = findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(_countdownFragment.isVisible())
                    return;

                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.flFragment, _countdownFragment).addToBackStack(null);
                ft.commit();
            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        _gestureDetectorCompat.onTouchEvent(event);
        return true;
    }

    public void displayMessage(String message) {

    }
}
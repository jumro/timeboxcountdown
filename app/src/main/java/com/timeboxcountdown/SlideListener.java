package com.timeboxcountdown;

import android.view.GestureDetector;
import android.view.MotionEvent;

public class SlideListener extends GestureDetector.SimpleOnGestureListener {

    private static int MIN_SWIPE_DISTANCE_Y = 50;
    private static int MAX_SWIPE_DISTANCE_Y = 2000;
    private MainActivity _activity;

    public SlideListener(MainActivity _activity) {
        this._activity = _activity;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        float deltaY = e1.getY() - e2.getY();
        float deltaYAbs = Math.abs(deltaY);

        this._activity.displayMessage("Delta is " + deltaY);

        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        this._activity.displayMessage("Single Tap");
        return true;
    }

}

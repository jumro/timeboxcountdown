package com.timeboxcountdown;

import android.os.Bundle;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

public class SettingsFragment extends Fragment implements IView {
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;
    private TextView scrollableTextView;
    private TextView countdownTimeTextView;

    public SettingsFragment() {
    }

    public static SettingsFragment newInstance(String param1) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        scrollableTextView = (TextView) view.findViewById(R.id.scrollable_time_configuration);
        scrollableTextView.setText(getStringOfTimeConfig());
        scrollableTextView.setOnClickListener(myHandler);
        scrollableTextView.setMovementMethod(new ScrollingMovementMethod());

        countdownTimeTextView = (TextView) view.findViewById(R.id.countdownTimeTextView);

        return view;
    }

    public String getStringOfTimeConfig() {
        StringBuilder timeConfigStringBuilder = new StringBuilder();

        for (int hours = 0; hours < 60; hours++) {
            for (int seconds = 0; seconds < 60; seconds++) {
                if (hours < 10)
                    timeConfigStringBuilder.append("0");
                timeConfigStringBuilder.append(String.valueOf(hours)).append(":");

                if (seconds < 10)
                    timeConfigStringBuilder.append("0");
                timeConfigStringBuilder.append(String.valueOf(seconds));
                timeConfigStringBuilder.append("\n");
            }

        }
        return timeConfigStringBuilder.toString();
    }

    View.OnClickListener myHandler = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.scrollable_time_configuration:

                    int scrollY = scrollableTextView.getScrollY();
                    int lineIdx = scrollableTextView.getLayout().getLineForVertical(scrollY);

                    int offsetBetweenTopAndMid = 4;
                    int counterTime = lineIdx + offsetBetweenTopAndMid;

                    countdownTimeTextView.setText(String.valueOf(counterTime));

            }
        }
    };

}
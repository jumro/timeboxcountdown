# Introduction

This is a project for a native Android App provides an simple and user-friendly interval timer.
The main focus is on lean-coffee meetings as well as scrum time box meetings.

# Reference links

- [Lean coffee facilitators guide](https://medium.com/agile-outside-the-box/lean-coffee-facilitator-s-guide-d79d9f13d0a9)
- [Timebox: Definition and Origins](https://www.agilealliance.org/glossary/timebox/)

To get a basic knowlage of time boxes and lean coffee techniques,check out the references above.

# Getting started

First thing is to follow the [Android tutorial](https://developer.android.com/training/basics/firstapp/) and
get Android Studio installed on your machine, so you can do development using
the Android IDE. Other IDE options are possible, but not directly described or
supported here. If you're using your own IDE, it should be fairly straightforward
to convert these instructions to use with your preferred toolchain.

## What's contained in this project

### Android code

To get a basic understanding of the project, have a look at the concept files in the folder docs/concept


